// This document contain all the endpoints for our application (also http methods)

const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController.js")

router.get("/viewTasks", (req, res) => {
	taskController.getAllTask().then(resultFromController => res.send(resultFromController))
})


router.post("/addNewTask", (req, res) => { 
	taskController.createTask(req.body).then(result => res.send(result))

});
module.exports = router;
						// :id will serve as our wildcard
router.delete("/deleteTask/:id" , (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})




router.put("/updateTask/:id", (req, res) => {	// req.params.id will be the basis of what document we will update
		// re.body the new document/contents 
	taskController.updateTask(req.params.id,req.body).then(result => res.send(result));
})
module.exports = router;


/*
2. Create a route 
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.

[PUT - Update - Wildcard required]
5. Create a controller function for changing the status of a task to "complete".
6. Create a route
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
*/


router.get("/:id", (req, res) => {
    taskController.retrieveTask(req.params.id).then(result => res.send(result));
})


router.put("/:id/complete", (req, res) => {
	taskController.changeTask(req.params.id).then(result => res.send(result));
})


