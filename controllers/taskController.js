// This document contains our app feature in displaying and manipulating our database

const Task = require ("../models/task.js");

module.exports.getAllTask = () => {
	return Task.find({}).then(result =>{
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false; // "Error detected";
		}
		else {
			return task;
		}
	})
}

// taskID parameter will serve as storage of ID from our URL/link
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}

	})
};


module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updateTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr);
				return false;
			}
			else {
				return updateTask;
			}
	
		})
	})
};


/*
[GET -Wildcard required]
1. Create a controller function for retrieving a specific task.
2. Create a route 
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.

[PUT - Update - Wildcard required]
5. Create a controller function for changing the status of a task to "complete".
6. Create a route
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

*/

module.exports.retrieveTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
};


module.exports.changeTask = (taskId) => {
	return Task.findByIdAndUpdate(taskId).then((result,error) => {
		if (error){
			console.log(error);
			return false;
		}
		result.status = "complete";
		return result.save().then((changeTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr);
				return false;
			}
			else {
				return changeTask;
			}
	
		})
	})
};

