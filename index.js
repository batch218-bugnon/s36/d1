/*
	Terminal: 
		npm init -y
		npm i express
		npm i mongoose
		touch .gitignore >> content: node_modules
*/

// dependencies
const express = require ("express");
const mongoose = require ("mongoose");


// models folder >> task.js
// controllers folder >> taskController.js
// route folder >> taskRoute.js

const taskRoute = require("./routes/taskRoute.js");

// Server setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// DB connection
mongoose.connect("mongodb+srv://admin:admin@b218-to-do.qu7lkpj.mongodb.net/toDo?retryWrites=true&w=majority", 
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use("/tasks", taskRoute);

app.listen(port, ()=> console.log(`Now listening to port ${port}
	`));

// 






























